<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'unauthorized' => 'Giriş icazəniz yoxdur.',
    'failed' => 'Daxil etdiyiniz məlumatlar doğru deyil.',
    'success' => 'Uğurlu!',
    'logout' => 'Uğurla çıxış olundu.',
    'throttle' => 'Çoxlu giriş cəhdləri. Zəhmət olmasa :seconds saniyədən sonra təkrar yoxlayın.',
    'login_not_found' => 'İstifadəçi adı mövcud deyil.',
    'invalid_password' => 'Şifrə doğru deyil.',
    'success_login' => 'Giriş uğurla tamamlandı!',
    'access_denied' => 'Girişiniz qadağandır!',
    'same_password' => 'Təkrar şifrə cari şifrə ilə eyni ola bilməz!',
    'password_change_success' => 'Şifrə uğurla dəyişdirildi.'

];
