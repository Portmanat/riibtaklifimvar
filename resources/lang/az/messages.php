<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Response messages
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the custom messages
    |
    */

    'user_registered' => 'İstifadəçi uğurla qeydiyyatdan keçdi.',
    'entered_information_incorrect' => 'Daxil edilmiş məlumatlar doğru deyil.',
    'success' => 'Şəxsi məlumatlarınız uğurla dəyişdirildi.',
    'file_uploaded' => 'Fayl yükləndi.',
    'image_uploaded' => 'Şəkil yükləndi.',
    'key_not_found' => ':key Tapılmadı',
    'not_found' => 'Məlumat Tapılmadı.',
    'offer_success' => 'Təklifiniz qeydiyyata alındı.',
    'success_registration' => 'Qeydiyyat uğurla tamamlandl.',
    'already_liked' => 'Bu təklifi artıq bəyənmisiniz.',
    'offer_like_success' => 'Bəyənildi',
    'result_like_success' => 'Bəyənildi',
    'success_offer_update' => 'Təklifiniz uğurla yeniləndi.'
];
